package uz.azn.mixedtopic

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import uz.azn.mixedtopic.databinding.RowItemBinding

class RowViewHolder :RecyclerView.Adapter<RowViewHolder.ViewHolder>(){

   private val elements = ArrayList<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    return ViewHolder(RowItemBinding.inflate(LayoutInflater.from(parent.context),parent,false))
    }

    override fun getItemCount(): Int = elements.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
     holder.onBind(elements[position])
    }

    inner class ViewHolder(private val binding: RowItemBinding):RecyclerView.ViewHolder(binding.root){
        fun  onBind(element:User){

            with(binding){
                firstName.text = element.firstName
                lastName.text = element.lastName
            }
        }
    }

   fun getData(userElement:MutableList<User>){
       elements.apply { clear(); addAll(userElement) }
   }


}