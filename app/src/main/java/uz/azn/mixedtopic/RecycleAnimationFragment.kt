package uz.azn.mixedtopic

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import uz.azn.mixedtopic.databinding.FragmentRecycleAnimationBinding


class RecycleAnimationFragment : Fragment(R.layout.fragment_recycle_animation) {
    private lateinit var binding: FragmentRecycleAnimationBinding
    private val rowViewHolder = RowViewHolder()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentRecycleAnimationBinding.bind(view)
        setData()
        with(binding) {
            recycleView.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = rowViewHolder

            }
        }
    }

    private fun setData() {
        val userList = mutableListOf<User>()
        var counter = 1
        for (i in 0 until 25) {
            userList.add(User("FirstName $counter", "LastName $counter"))
            counter++
        }
        rowViewHolder.getData(userList)
    }
}