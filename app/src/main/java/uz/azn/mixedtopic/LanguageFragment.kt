package uz.azn.mixedtopic

import android.app.AlertDialog
import android.content.Context
import android.content.res.Configuration
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import android.widget.Toolbar
import androidx.fragment.app.Fragment
import uz.azn.mixedtopic.databinding.FragmentLanguageBinding
import java.util.*

class LanguageFragment : Fragment(R.layout.fragment_language), Toolbar.OnMenuItemClickListener,
    androidx.appcompat.widget.Toolbar.OnMenuItemClickListener {
    private lateinit var binding: FragmentLanguageBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getLanguage()
        binding = FragmentLanguageBinding.bind(view)

        with(binding) {
            toolbarLanguage.apply {
                title = requireContext().resources.getString(R.string.app_name)
                setOnMenuItemClickListener(this@LanguageFragment)

            }

        }


    }

    override fun onMenuItemClick(item: MenuItem?): Boolean = when (item?.itemId) {
        R.id.language -> {
            Toast.makeText(requireContext(), "Clicked", Toast.LENGTH_SHORT).show()
            openLanguageDialog()
            true
        }
        else -> {
            false
        }
    }

    private fun openLanguageDialog() {
        val languages = arrayOf("English", "Русский", "Ozbek tili")
        val builder = AlertDialog.Builder(requireContext())
            .setTitle(getString(R.string.choose_language))
            .setSingleChoiceItems(languages, getLanguageCheckedId()) { dialog, which ->
                if (which == 0) {
                    setLanguageId(which)
                    setLanguage("eng")
                    requireActivity().recreate()
                } else if (which == 1) {
                    setLanguageId(which)
                    setLanguage("ru")
                    requireActivity().recreate()

                } else if (which == 2) {
                    setLanguageId(which)
                    setLanguage("uz")
                    requireActivity().recreate()

                }
                dialog.dismiss()
            }
        val dialog = builder.create()
        dialog.show()


    }

    fun setLanguage(lang: String) {
        val locale = Locale(lang)
        Locale.setDefault(locale)
        val confic = Configuration()
        confic.locale = locale
        requireActivity().baseContext.resources.updateConfiguration(
            confic,
            requireActivity().baseContext.resources.displayMetrics
        )
        val editor =
            requireContext().getSharedPreferences("language_setting", Context.MODE_PRIVATE).edit()
        editor.putString("language", lang)

        editor.apply()
    }

    fun getLanguage() {
        val preferences =
            requireContext().getSharedPreferences("language_setting", Context.MODE_PRIVATE)
        val language = preferences.getString("language", "en")
        setLanguage(language!!)
    }

    fun getLanguageCheckedId(): Int {
        val preferences =
            requireContext().getSharedPreferences("language_setting", Context.MODE_PRIVATE)
        return preferences.getInt("checkedItem", 0)

    }

    fun setLanguageId(checkedId: Int) {
        val editor =
            requireContext().getSharedPreferences("language_setting", Context.MODE_PRIVATE).edit()
        editor.putInt("checkedItem", checkedId)
        editor.apply()
    }
}